package com.example.android.sunshine;

import android.content.Intent;
import android.support.v4.app.ShareCompat;
import android.support.v4.app.SharedElementCallback;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.Set;

public class DetailActivity extends AppCompatActivity {

    private static final String FORECAST_SHARE_HASHTAG = " #SunshineApp";
    private TextView tvWeatherData;
    private String forecast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        tvWeatherData = (TextView) findViewById(R.id.tvDetailedWeatherData) ;

        Intent startedIntent = getIntent();

        if(startedIntent != null){
            if(startedIntent.hasExtra(Intent.EXTRA_TEXT)){
                forecast = startedIntent.getStringExtra(Intent.EXTRA_TEXT);
                tvWeatherData.setText(forecast);
            }
        }
    }

    //create the share intent
    private Intent createShareForecastIntent(){
        Intent shareIntent = ShareCompat.IntentBuilder.from(this)
                .setType("text/plain")
                .setText(forecast+FORECAST_SHARE_HASHTAG)
                .getIntent();
        return shareIntent;
    }

    //share if the user press the share button
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.detail,menu);
        MenuItem item = menu.findItem(R.id.action_share);
        item.setIntent(createShareForecastIntent());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch(id){
            case R.id.action_settings:
                Intent settingsIntent = new Intent(DetailActivity.this, SettingsActivity.class);
                startActivity(settingsIntent);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
