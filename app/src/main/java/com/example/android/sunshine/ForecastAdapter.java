package com.example.android.sunshine;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.ForecastAdapterViewHolder> {
    private String[] weatherData;

    private final ForecastAdapterOnClickHandler clickHandler;

    public ForecastAdapter(ForecastAdapterOnClickHandler handler){
        this.clickHandler = handler;
    }

    public interface ForecastAdapterOnClickHandler{
        void onClick(String whichDay);
    }

    @Override
    public ForecastAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForListItem = R.layout.forecast_list_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem,parent,shouldAttachToParentImmediately);

        return new ForecastAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ForecastAdapterViewHolder holder, int position) {
        String todaysWeather = weatherData[position];
        holder.tvWeatherData.setText(todaysWeather);
    }

    @Override
    public int getItemCount() {
        if(weatherData == null) return 0;
        return weatherData.length;
    }

    public void setWeatherData(String[] weatherDataInput){
        weatherData = weatherDataInput;
        notifyDataSetChanged();
    }

    public class ForecastAdapterViewHolder extends RecyclerView.ViewHolder
        implements View.OnClickListener{

        public final TextView tvWeatherData;

        public ForecastAdapterViewHolder(View view){
            super(view);
            tvWeatherData = (TextView) view.findViewById(R.id.tvWeatherData);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v){
            int clickedPosition = getAdapterPosition();
            String dayWeather = weatherData[clickedPosition];
            clickHandler.onClick(dayWeather);
        }
    }
}
