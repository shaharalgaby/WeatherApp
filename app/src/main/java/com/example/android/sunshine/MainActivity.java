/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.android.sunshine;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.android.sunshine.data.SunshinePreferences;
import com.example.android.sunshine.utilities.NetworkUtils;
import com.example.android.sunshine.utilities.OpenWeatherJsonUtils;

import java.net.URL;

public class MainActivity extends AppCompatActivity implements
        ForecastAdapter.ForecastAdapterOnClickHandler,
        LoaderCallbacks<String[]>,
        SharedPreferences.OnSharedPreferenceChangeListener{

    private RecyclerView rvForecast;
    private ForecastAdapter forecastAdapter;
    private TextView tvErrorMsg;
    private ProgressBar progressBar ;
    private static boolean PREFERENCES_HAVE_BEEN_UPDATED = false;
    private static final int FORECAST_LOADER_ID = 0;


    @Override
    protected void onDestroy() {
        super.onDestroy();

        PreferenceManager.getDefaultSharedPreferences(this)
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(PREFERENCES_HAVE_BEEN_UPDATED) {
            getSupportLoaderManager().restartLoader(FORECAST_LOADER_ID,null,this);
            PREFERENCES_HAVE_BEEN_UPDATED = false;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forecast);

        rvForecast = (RecyclerView) findViewById(R.id.rvForecast);
        tvErrorMsg = (TextView) findViewById(R.id.tvErrorMsg);
        progressBar = (ProgressBar) findViewById(R.id.prLoadingWeather);

        LinearLayoutManager layoutManager = new LinearLayoutManager(
                this,LinearLayoutManager.VERTICAL,false);

        rvForecast.setLayoutManager(layoutManager);

        rvForecast.setHasFixedSize(true);

        forecastAdapter = new ForecastAdapter(this);

        rvForecast.setAdapter(forecastAdapter);

        getSupportLoaderManager().initLoader(FORECAST_LOADER_ID, null, MainActivity.this);

        PreferenceManager.getDefaultSharedPreferences(this)
                .registerOnSharedPreferenceChangeListener(this);
    }

    /*
    Inflates the menu to the activity
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();

        inflater.inflate(R.menu.forecast,menu);

        return true;
    }

    /*
    Handles click on the refresh button
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch(id) {
            case  R.id.action_refresh:
                invalidateData();
                getSupportLoaderManager().restartLoader(FORECAST_LOADER_ID, null, this);
                return true;


            case R.id.action_map:
                openLocationInMap();
                return true;

            case R.id.action_settings:
                Intent startSettingsIntent = new Intent(MainActivity.this,SettingsActivity.class);
                startActivity(startSettingsIntent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openLocationInMap() {
        String addressString = SunshinePreferences.getPreferredWeatherLocation(this);
        Uri geoLocation = Uri.parse("geo:0,0?q=" + addressString);

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(geoLocation);

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Log.d("ERROR", "Couldn't call " + geoLocation.toString()
                    + ", no receiving apps installed!");
        }
    }

    //The interface overridden method
    @Override
    public void onClick(String whichDay) {
        Context context = this;
        Intent detailedActivity = new Intent(context,DetailActivity.class);
        detailedActivity.putExtra(Intent.EXTRA_TEXT,whichDay);
        startActivity(detailedActivity);
    }

    //set weather to visible if there is no error
    public void showWeatherDataView(){
        rvForecast.setVisibility(View.VISIBLE);
        tvErrorMsg.setVisibility(View.INVISIBLE);
    }

    //if an error occured show message
    public void showErrorMessage(){
        rvForecast.setVisibility(View.INVISIBLE);
        tvErrorMsg.setVisibility(View.VISIBLE);
    }

    /***********************************************************************************************
                        This is the actual action to do in background
     ***********************************************************************************************/
    @SuppressLint("StaticFieldLeak")
    @Override
    public Loader<String[]> onCreateLoader(int id, final Bundle args) {
        return new AsyncTaskLoader<String[]>(this) {

            String[] weatherData = null;

            //deliver the cached data
            @Override
            protected void onStartLoading(){
                if(weatherData != null){
                    deliverResult(weatherData);
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                    forceLoad();
                }
            }

            //Method that will load the data from the internet.
            @Override
            public String[] loadInBackground() {
                String location = SunshinePreferences.getPreferredWeatherLocation(
                        MainActivity.this);

                String[] jsonWeatherData;
                URL weatherURL = NetworkUtils.buildUrl(location);

                try{
                    String jsonWeatherResponse = NetworkUtils.getResponseFromHttpUrl(weatherURL);
                    jsonWeatherData = OpenWeatherJsonUtils
                            .getSimpleWeatherStringsFromJson(MainActivity.this, jsonWeatherResponse);

                    return jsonWeatherData;

                }catch(Exception e){
                    e.printStackTrace();
                    return null;
                }
            }

            public void deliverResult(String[] data) {
                weatherData = data;
                super.deliverResult(data);
            }
        };
    }
    /*************************************Finish of onCreateLoader******************************************/

    @Override
    public void onLoadFinished(Loader<String[]> loader, String[] data) {
        progressBar.setVisibility(View.INVISIBLE);
        forecastAdapter.setWeatherData(data);

        if(data != null)
            showWeatherDataView();
        else
            showErrorMessage();
    }

    /**
     * Called when a previously created loader is being reset, and thus
     * making its data unavailable.  The application should at this point
     * remove any references it has to the Loader's data.
     *
     * @param loader The Loader that is being reset.
     */
    @Override
    public void onLoaderReset(Loader<String[]> loader) {
        //Implementation needed.
    }

    public void invalidateData(){
        forecastAdapter.setWeatherData(null);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        PREFERENCES_HAVE_BEEN_UPDATED = true;
    }
}