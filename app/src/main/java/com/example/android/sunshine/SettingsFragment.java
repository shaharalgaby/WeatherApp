package com.example.android.sunshine;

import android.os.Bundle;
import android.support.v7.preference.CheckBoxPreference;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.content.SharedPreferences;
import android.support.v7.preference.PreferenceScreen;

public class SettingsFragment extends PreferenceFragmentCompat
        implements SharedPreferences.OnSharedPreferenceChangeListener {

    public void setPreferenceSummary(Preference preference,Object val){
        String stringVal = val.toString();
        String key = preference.getKey();

        if(preference instanceof ListPreference){
            ListPreference prefList = (ListPreference)preference;
            int prefIndex = prefList.findIndexOfValue(stringVal);
            if(prefIndex>=0)
                preference.setSummary(prefList.getEntries()[prefIndex]);
        }
        else
            preference.setSummary(stringVal);

    }

    @Override
    public void onStart() {
        super.onStart();
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onStop(){
        super.onStop();
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);

    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Preference preference = findPreference(key);
        if(preference != null){
            if(!(preference instanceof CheckBoxPreference)){
                setPreferenceSummary(preference,sharedPreferences.getString(key,""));
            }
        }
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.pref_general);

        PreferenceScreen preferenceScreen = getPreferenceScreen();
        SharedPreferences sharedPreferences = preferenceScreen.getSharedPreferences();

        int count = preferenceScreen.getPreferenceCount();

        for(int i = 0 ; i < count ; i++){
            Preference pref = preferenceScreen.getPreference(i);
            if(!(pref instanceof CheckBoxPreference)){
                String val = sharedPreferences.getString(pref.getKey(),"");
                setPreferenceSummary(pref, val);
            }
        }
    }
}
